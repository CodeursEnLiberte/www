+++
date = "2016-09-04T18:42:31+02:00"
title = "Codeur·euses en Liberté"
no_date = true
no_title = true
+++


# Codeur·euses en Liberté est une coopérative d’indépendant·es en informatique.
  
<section>

Depuis 2016, Codeur·euses en Liberté rassemble une dizaine de personnes au sein d’un collectif. Nous intervenons dans tous les domaines autour du web : du frontend et backend, du design à l’administration système. Nous réalisons des produits complets, du prototype à la mise en production et la maintenance à long terme. Il nous arrive de faire démarrer de petits projets naissants, tout comme d’intervenir au sein de grandes équipes.

</section>
<section>

En général, nous aimons bien les technologies éprouvées. Concrètement, nous travaillons beaucoup avec Ruby on Rails, Rust, Python, Elixir, PostgreSQL, Git, Github et Gitlab, Docker, Ansible, Grist, QGis… et évidemment, du HTML, CSS et du javascript.

</section>
<section id="clients" class="wrapped-list-container">

Nous parlons souvent de transports en commun et de vélo, de politiques publiques, d’accès et droit, et de vie privée, de transparence, d’accessibilité et de sécurité. Depuis le début, nous avons travaillé avec des acteurs publics et institutionnels, des coopératives et associations, et des structures privées :

- [beta.gouv.fr](https://beta.gouv.fr)
- [transport.data.gouv.fr](https://transport.data.gouv.fr)
- [le.taxi](le.taxi)
- [Recosanté](https://recosante.beta.gouv.fr) 
- [demarches-simplifiees.fr](https://www.demarches-simplifiees.fr)
- [data.gouv.fr](https://www.data.gouv.fr/fr/)
- [RDV-service-public](https://rdv.anct.gouv.fr)
- [Place des Entreprises](https://conseillers-entreprises.service-public.fr/)
- [1jeune1solution](https://www.1jeune1solution.gouv.fr)
- [mes-aides.gouv.fr](https://mes-aides.gouv.fr)
- [Aidants Connect](https://aidantsconnect.beta.gouv.fr)
- [RATP](https://www.ratp.fr)
- [SNCF Réseau](https://www.sncf-reseau.com/fr)
- [La fabrique des géocommuns](https://www.ign.fr/institut/la-fabrique-des-geocommuns-incubateur-de-communs-lign)
- [Panoramax](https://panoramax.fr)
- [Référentiel National des Bâtiments](https://rnb.beta.gouv.fr)
- [Eurométropole de Strasbourg](https://www.strasbourg.eu)
- [enercoop](https://www.enercoop.fr)
- [Cargonautes](https://www.cargonautes.fr) 
- [Collectif Vélo Île-de-France](https://velo-iledefrance.fr)
- [leboncoin](https://www.leboncoin.fr)
- [kisio](https://kisio.com)
- [Deepki](https://www.deepki.com/fr/)
- [Deezer](https://www.deezer.com/fr/)
- [Géovélo](https://geovelo.app)
- [Ada Tech School](https://adatechschool.fr)
- [Brut](https://www.brut.media/fr)
- [Fretlink](https://www.fretlink.com/fr/) 
- [Plateforme de l’inclusion](https://inclusion.beta.gouv.fr)
- [Amnesty International](https://www.amnesty.org/fr/)

</section>
<section>
<img src="/img/cocarto-carre-couleur.svg" alt="logo cocarto" style="width: 130px; margin: 8px 1em 0 0; float: left;">
  
Depuis quelques années, [cocarto](https://cocarto.com/presentation) occupe aussi une partie de notre temps. C’est un outil de saisie collaborative de données structurées et géospatialisées, libre et open-source, à destination de toutes les personnes qui partagent des données *à caractère géographique*.

</section>
<section>
<img src="/img/codecodecoop.webp" alt="logo codecodecoop" style="width: 130px; margin: 8px 0 0 1em; float: right;">

[Nous sommes une coopérative](/a-propos/qui) : la gouvernance est démocratique et transparente, et les décisions sont prises ensemble. Nous avons aussi établi une [charte d’éthique](/blog/ethique/charte) qui guide notre pratique et notre responsabilité en tant que professionnels du numérique.

</section>
<section>
  
Contactez-nous par email à <svg class="icon"><use href="/img/icons.svg#fa-envelope"/></svg> bonjour@codeureusesenliberte.fr, sur <svg class="icon"><use href="/img/icons.svg#fa-mastodon"/> </svg> <a href="https://framapiaf.org/@CodeursEnLiberte">Mastodon</a> ou <svg class="icon"><use href="/img/icons.svg#fa-signal-messenger"/></svg> <a href="https://signal.group/#CjQKIPFG_O2YMPhoVeo-h9FSGKs_3AwN1mxjun86RxMSLVbmEhAjubKQIdMGLv_ro8RGYe-p">sur Signal</a>.

</section>
