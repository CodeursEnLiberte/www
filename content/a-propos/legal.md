+++
date = "2016-11-05T20:01:41+02:00"
title = "Mentions légales"
no_date = true
+++

<section id="mentions-legales">

<dl>
<dt>Codeur·euses en Liberté</dt>
<dd>SAS coopérative au capital variable de 8200 €</dd>
<dd>118 avenue Jean Jaurès, 75019 Paris</dd>
<dt>SIRET</dt><dd>821611431 00031</dd>
<dt>Responsable de publication</dt><dd>Francis Chabouis</dd>
<dt>Contact</dt>
<dd><a href="bonjour@codeureusesenliberte.fr">bonjour@codeureusesenliberte.fr</a></dd>
</dl>

</section>
<section>
  
## Hébergement

Ce site est hébergé sur [GitLab Pages](https://about.gitlab.com/company/visiting/).

</section>
<section>
  
## 🍪 Cookies et données personnelles

Nous ne plaçons aucun cookie sur votre navigateur et nous ne transmettons à des tiers aucune information vous concernant. Nous n’avons pas accès au journal d’accès au serveur http. Cependant, GitLab pourrait conserver vos données de connexion :
* Horodotage,
* Adresse IP,
* Page accédée,
* _User-Agent_ de votre navigateur.

</section>
<section>
  
## Licence et charte d’éthique

L’ensemble du contenu du site est réutilisable en respectant les conditions de [la licence CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/fr/). Cette licence concerne ce que nous publions. Vous ne pouvez cependant pas vous faire passer pour la coopérative Codeur·euses en Liberté.

Notre activité est soumise au respect de [notre charte d’éthique](/blog/ethique/charte/).

</section>
