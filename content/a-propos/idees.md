+++
date = "2024-03-29T13:00:00+01:00"
title = "Idées"
no_date = true
+++

Voici des choses qui sont en discussion en ce moment chez nous, certaines seront réalisées, d’autres non, et c’est ok 🤷.

[Modifier cette page](https://gitlab.com/CodeursEnLiberte/www/-/edit/main/content/a-propos/idees.md)

## Choses à réaliser

### Calendrier révolutionnaire

Tristram code sur un projet de calendrier révolutionnaire en rust. On est le premier frimaire aujourd’hui, et il neige.

### Nouveau site web.

On peut faire un ou deux blogposts sur les technos: on a fait un ou deux trucs rigolos en css, et sur les icônes en SVG.

### Devenons adulte

On voudrait réaliser au moins un poste de blog pour dire que l’on ne finance pas les outils open-source avec lesquels on travaille et que ça serait bien qu’on devienne un peu responsable et que l’on commence à payer au lieu de se voiler la face.

### PMTiles de la France

Écrire une bibliothèque qui permette de représenter une carte de France, pas que métropolitaine, de manière aussi jolie si possible.

## Choses réalisées

### Un nouveau design pour le site web

Il reste des trucs, mais c’est vachement mieux.

### Grille d’interviews pour codecode.coop

Actuellement le recueil de témoignage (codecode.coop) n’est pas optimal, faire une grille d’interview nous permettrai peut être de faciliter ce recueil.
