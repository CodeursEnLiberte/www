+++
date = "2016-09-04T20:01:41+02:00"
title = "Qui sommes-nous ?"
no_date = true
+++

**Que sommes-nous ?** Codeur·euses en Liberté n’est pas un projet très précis, pas tout à fait une _entreprise de services du numérique_, certainement pas une start–up. C’est un collectif d’individus, avec des envies communes et des idées fortes sur la place du web et du numérique dans la société.

<section class="hero">
  <img src="/blog/semaine-en-commun-2019/groupe.webp" alt="Les membres de la coopérative, tout sourires.">
</section>


## Militantisme numérique

Ça ne va pas de soi dans notre industrie : nous voulons travailler sur des projets auxquels nous croyons. Évidemment, nous serions _moins bons_ en travaillant sur des projets qui ne nous plaisent pas, mais ce n’est pas de ça qu’il s’agit. Parce que nous savons que le numérique n’est pas uniquement un reflet de la société, parce que ce qui se passe dans le web a une influence primordiale sur les personnes, nous voulons _faire mieux_.

En somme, nous voulons, par notre métier, faire bouger les lignes dans le bon sens, et nous considérons que c’est notre responsabilité. Nous avons rédigé et adopté une **[charte d’éthique](/blog/ethique/charte)**, qui guide notre pratique. Pour commencer, nous refusons de travailler pour l’industrie de l’armement et privilégions le logiciel libre, mais c’est plus que ça. Il s’agit d’éviter de manipuler les usagers, de ne pas discriminer, et de protéger la vie privée. À l’heure des LLM, des crypto-masculinistes et du capitalisme de surveillance, la charte nous permet de définir ce que nous refusons de faire dans notre pratique de professionnels du numérique.

Concrètement, nous privilégions les clients à impact social positif (les coopératives, associations militantes, service public…) en réduisant notre facture. Il nous arrive régulièrement de faire grève et de participer à des manifs; quand ça arrive, nous mettons en place un bandeau _grève_ [sur le site](https://gitlab.com/CodeursEnLiberte/www/-/merge_requests/?sort=created_date&state=merged&search=grève&first_page_size=20). Lors des législatives anticipées de 2024, nous avons participé à la mise en place du site [frontpopulaire.coop](https://frontpopulaire.coop), en soutien au nouveau front populaire. Depuis peu, nous essayons de [rémunérer](https://gitlab.com/CodeursEnLiberte/fondations/-/issues/215) les outils que nous utilisons, notamment avec du sponsoring en reversant 1% de notre CA.

## Commune d’indépendant·es

Travailler seul·e en freelance, c’est très bien – mais c’est un peu usant à la longue. Au-delà d’une simple structure administrative, Codeur·euses en Liberté est aussi un espace pour travailler ensemble, s’entraider, partager des connaissances et des photos de chiens, et se serrer les coudes en cas de coup dur. *Solidaires plutôt que solitaires*.

L’indépendance et l’autonomie dans le travail sont essentielles pour nous mais elles sont, en général, incompatibles avec le statut de salarié·e. La protection sociale des indépendant·e·s n’offre pas les mêmes garanties que le régime salarié. En nous regroupant dans une structure commune, nous bénéficions des avantages du salariat, tout en gardant la liberté de notre temps de travail et de nos missions.

Nous sommes attachés à un équilibre entre les différents parties de la vie. Ainsi, nous travaillons autant, ou _aussi peu_ que nous le voulons. Dans les faits, la plupart d’entre nous travaillent quatre jours par semaine.

## Coopératifs

Chaque salarié·e de la coopérative est aussi sociétaire. Nous sommes une coopérative au sens de la [loi de 1947](https://www.legifrance.gouv.fr/loda/id/JORFTEXT000000684004/2020-11-03) et des [principes coopératifs](https://ica.coop/fr/coopératives/identite-cooperative#toc-2-contr-le-d-mocratique-exerc-par-les-membres): cela ne nous donne pas d’avantage fiscal particulier, mais chaque sociétaire — et donc, chaque salarié·e – a le même pouvoir sur les décisions du collectif.

Nous ne cherchons pas à grandir — notre mode de fonctionnement est adapté pour une dizaine de personnes, pas beaucoup plus — mais nous sommes très attachés à ce mode de gouvernance, et nous essayons de l’encourager autour de nous. Nous avons recueilli le témoignage de quelques structures amies sur [codecode.coop](https://codecode.coop): chacune a son propre fonctionnement quotidien, résultat de la volonté de ses membres. Les principes coopératifs peuvent en fait s’articuler de nombreuses façons.

Dans cet esprit, nous sommes [_ouverts par défaut_](https://en.wikipedia.org/wiki/Open_by_Default) pour partager notre expérience. Nous pensons que la transparence devrait être la norme, parce qu’elle incite à l’honnêteté dans les rapports humains. La majeure partie de nos réflexions d’entreprise, de nos discussions sont visibles [sur gitlab, sous formes d’issues](https://gitlab.com/CodeursEnLiberte/fondations), ainsi que les décisions de nos assemblées générales. En fait, seules les négociations commerciales et les données personnelles ne sont pas publiques.

<section>

## Les codeur·euses

<div class="bios">
  
{{% bio name="KheOps" pic="kheops.webp" id="kheops" hearts="Données personnelles et vie privée, hébergement sécurisé pour petites organisations, audits et formations en sécurité" %}}
{{< email kheops >}}
{{< gpg "D1F546E378CCFAB67B21C79ABA5B6E9F" "53BB2174" "kheops@ceops.eu.asc" >}}
{{< github kheops2713 >}}
{{< gitlab kheops2713 >}}
{{< mastodon kheops2713 "piaille.fr" >}}
{{% /bio %}}

{{% bio name="Tristram Gräbener" pic="tristram.webp" id="tristram" hearts="Cartographie, transports, données ouvertes" %}}
{{< email tristram >}}
{{< web tristramg.eu >}}
{{< github tristramg >}}
{{< gitlab tristramg >}}
{{< mastodon tristramg "mamot.fr" >}}
{{% /bio %}}

{{% bio name="Pierre de La Morinerie" pic="pierre.webp" id="pierre" hearts="Ruby on Rails, les tests qui passent vite, le web décentralisé" %}}
{{< email pierre >}}
{{< github kemenaran >}}
{{< gitlab pmorinerie >}}
{{< mastodon pmorinerie "mastodon.xyz" >}}
{{% /bio %}}

{{% bio name="Vincent Lara" pic="vincent.webp" id="vincent" hearts="API, transport, backend" %}}
{{< email vincent >}}
{{< github l-vincent-l >}}
{{< gitlab l-vincent-l >}}
{{< mastodon vincent "mamot.fr" >}}
{{% /bio %}}

{{% bio name="Nicolas Bouilleaud" pic="nicolas.webp" pic_copyright="photo par Konstantin Kourenkov" id="nicolas" hearts="Les utilisateurs, l’accessibilité, les interfaces claires, les transports publics, les cartes et la mobilité" %}}
{{< email nicolas >}} {{< web bou.io >}} {{< gitlab n-b >}} {{< mastodon _nb "mamot.fr" >}}
{{% /bio %}}

{{% bio name="Thimy Kieu" pic="thimy.webp" id="thimy" hearts="Imaginer et construire de belles choses" %}}
{{< email thimy >}}
{{< github thimy >}}
{{< gitlab thimy >}}
{{% /bio %}}

{{% bio name="Francis Chabouis" pic="francis.webp" id="francis" hearts="Les cartes et les maths" %}}
{{< email francis >}}
{{< github fchabouis >}}
{{< gitlab fchabouis >}}
{{% /bio %}}

{{% bio name="Antoine Desbordes" pic="antoine.webp" id="antoine" hearts="Rust, transport" %}}
{{< email antoine>}}
{{< github antoine-de >}}
{{< gitlab antoine-de >}}
{{% /bio %}}

{{% bio name="Thibaut Sailly" pic="thibaut.webp" id="thibaut" hearts="Harmonie, typo, vélo, cuisine." %}}
{{< email thibaut >}}
{{< web tsailly.net >}}
{{% /bio %}}

{{% bio name="Thomas Guillet" pic="thomas.webp" id="thomas" hearts="L’accès au droit, les simulateurs de prestations sociales et les labyrinthes" %}}
{{< email thomas >}}
{{< github guillett >}}
{{< gitlab guillett >}}
{{% /bio %}}

{{% bio name="Enkhe Deny" pic="jay.webp" id="enkhe" hearts="mon chien et apprendre" %}}
{{< email enkhe >}}
{{< github enkhe-D >}}
{{< gitlab enkhe-D >}}
{{% /bio %}}

</div>
</section>

<section id="old">

## 👋 Anciens membres

<div class="bios">
  
{{% bio name="Baptiste Brun-Arasa" pic="baptiste.webp" id="baptiste" hearts="Apprendre, découvrir, C# et l’esport management. Strictement aucun rapport." %}}
{{< github baptisteb-a >}}
{{% /bio %}}

{{% bio name="Etienne Maynier" pic="etienne.webp" id="etienne" hearts="Les discussions politiques, les emails de phishing et le houmous" %}}
{{< github te-k >}}
{{% /bio %}}

{{% bio name="Nelson Perdriau" pic="nelson.webp" id="nelson" hearts="Logiciels Libres, Rust, Co-op, Données ouvertes, Agriculture" %}}{{% /bio %}}

{{% bio name="Judith Poulvelarie" pic="judith.webp" id="judith" hearts="La sobriété numérique, la Terre et le Soleil" %}}
{{< github jpoulvel >}}
{{% /bio %}}

{{% bio name="Heddy Bhm" pic="heddy.webp" id="heddy" hearts="C, Unix et les énigmes" %}}
{{< github bhm-heddy >}}
{{% /bio %}}

{{% bio name="Vanessa Ducloux" pic="vanessa.webp" id="vanessa" hearts="Apprendre, et les choses qui ont du sens" %}}
{{< github Vanessa-D >}}
{{% /bio %}}

{{% bio name="Hugo Guignard" pic="hugo.webp" id="Hugo" hearts="Le sport et la cyber." %}}
{{< github hugoguignardchab >}}
{{% /bio %}}

{{% bio name="Chris Ahona" pic="chris.webp" id="chris" hearts="Apprendre et approfondir Ruby, j’aime la production musicale et le foot." %}}
{{< github Chrisahn75 >}}
{{% /bio %}}

{{% bio name="Lydia Sanaa" pic="lydia.webp" id="lydia" hearts="le bricolage et la moto" %}}
{{< github lydiasan >}}
{{% /bio %}}

</div>
</section>
