+++
date = "2024-06-05T13:12:00+01:00"
title = "cocarto : billet d’étape n°13"
author = "Tristram"
categories = "cocarto"
+++

[cocarto](https://cocarto.com/) est un tableur de données géographiques. Vous lisez la treizième édition de la [lettre mensuelle](https://buttondown.email/cocarto) qui en raconte les développements, doutes, détails inutiles…

## Des sous

Vous avez remarqué : nos courriers se font rares. Cocarto a été developpé sur des fonds propres de [Codeureuses en Liberté](https://www.codeureusesenliberte.fr/). Les clients payants ont été moins nombreux que prévu et nous avons épuisé (et à vrai dire même dépassé) la somme que nous nous étions fixée.

Depuis nous continuons à travailler dessus, mais plus comme sur un logiciel libre communautaire, sur notre temps libre quand nous ne travaillons pas pour des clients qui nous payent.

Les développements vont continuer, à un rythme plus modeste, en nous concentrant sur les retours des utilisateurs et de tout ce qui gratte à l’utilisation.

Et bien sûr nous continueront à maintenir en ligne le service [cocarto.com](https://cocarto.com).

## Des imports

C’était un chantier qui a pris son temps. Mais les imports sont enfin plus performants !

Nous avons pensé cocarto comme un outil de saisie ; l’import manquait donc un peu d’attention. Il arrivait donc souvent que des données trop volumineuses finissent par planter.

Pour les personnes curieuses, le bug était principalement lié au fait que pour chaque nouvelle données, nous essayions de notifier toutes les personnes qui observaient la page. [Désormais](https://gitlab.com/CodeursEnLiberte/cocarto/-/merge_requests/743), l’import est fait par lots.

## Des élections et des cartes

Comme les deux tiers de la France, nous avons été estomaqués par les résultats de l’extrême-droite aux élections européennes et nous avons vu des cartes couleur marron.

Mais « les surfaces de votent pas, ce sont les humains ». En représentant les communes par des points de taille proportionnele à leur population, c’est une carte un peu moins déprimante qui se dessine. Merci à [Karim Douïeb (x.com)](https://x.com/karim_douieb/status/1800777148871188766) pour son travail de représentation.

![Carte des résultats en France des élections européennes 2024 avec les communes représentées par un point dont la taille dépend de la population.](/blog/cocarto_13/elections_europeennes.jpg)

Tout comme cette carte nuance un peu les résultats, en moins de trois semaines, la forte mobilisation a fait baisser la part de l’extrême droite de 36,84 % (RN+Reconquête) à 30,01 %, malgré le raliement d’une partie de LR. La mobilisation doit donc continuer.

En tant que coopérative, nous sommes bien sûr signataires de l’appel à [soutenir le nouveau front populaire](https://frontpopulaire.coop/) et nous appelons à s’opposer fermement à l’extrême-droite.

##

C’est tout pour cette fois-ci ! Merci encore de nous avoir lus. N’hésitez pas à nous envoyer vos commentaires sur [bonjour@cocarto.com](mailto:bonjour@cocarto.com), à regarder la [tickets ouverts](https://gitlab.com/CodeursEnLiberte/cocarto/-/issues/?sort=created_date&state=opened&first_page_size=100), et à nous signaler des bugs.
