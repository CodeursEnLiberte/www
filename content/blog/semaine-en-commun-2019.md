+++
date = "2019-10-06T15:00:00+02:00"
title = "Retour sur une semaine en commun"
author = "Antoine et Pierre"
categories = "Entreprise"
+++

Chaque année, les Libres Codeur·euse·s se retrouvent dans un même lieu pendant une petite semaine. C’est notre _séminaire_ annuel : autour d’un forum ouvert, c’est l’occasion d’échanger, de travailler sur des projets communs, d’améliorer notre organisation, et de passer du temps ensemble autour de balades et de jeux de sociétés.

<br>

![Photo de groupe ; sans Etienne, qui n’a pas pu venir.](/blog/semaine-en-commun-2019/groupe.jpg)

## Projets communs

En plus des siestes, de la cuisine et des parties de Mario Kart (Bowser, meilleur personnage), on a réussi à avancer sur quelques projets à nous.

Oui, parce que le séminaire est l’occasion pour certain·e·s d’entre nous de **ressortir les projets personnels** du tiroir, d’avancer un peu sur nos projets de long-terme, et de, pour une fois, partager notre enthousiasme avec d’autres gens. Entre autres envies, cette semaine a vu du progrès sur [Bicyclette](https://bicyclette.app), [LADX](https://github.com/zladx/LADX-Disassembly), et un enthousiasme commun pour le début d’[Inktober](https://inktober.com/).

C’est aussi un bon moment pour **partager nos connaissances** : nous mettre à jour sur les détails de notre comptabilité autogérée, ou discuter des subtilités de la perception des couleurs dans l’œil humain.

Mais c’est aussi l’occasion de lancer ou d’**avancer sur des projets de coopérative communs** à nous tous. Cette semaine, nous avons avancé sur :

- **[Ardoise](https://gitlab.com/CodeursEnLiberte/ardoise/)**, notre outil de suivi du chiffre d’affaire, avec une clarification de l’interface, et de nouvelles maquettes en préparation ;
- **[Chuis.la](http://chuis.la)**, un prototype de partage de position libre et sans pistage ;
- **[codecode.coop](https://codecode.coop)**, un guide pour créer sa coopérative dans l’informatique, traduit de l’excellente [version anglaise](https://www.techworker.coop/resources/technology-freelancers-guide-starting-worker-cooperative) du [NATWC](https://www.techworker.coop/).

## Décisions de groupe

Notre fonctionnement collectif peut toujours être remis en question et amélioré.

Dans les sujets de cette année, nous avons parlé de **congés maternité et paternité**. Nous avons décidé de compléter les indemnités octroyées par la sécu et la convention collective par un [salaire parental](lien vers l’issue), versé par la coopérative pour permettre de prendre plus de temps à la naissance d’un enfant.

Notre mode de rémunération organise une **solidarité entre membres**, où la contribution de chacun à la coopérative est [fonction du montant facturé](https://gitlab.com/CodeursEnLiberte/fondations/wikis/Fonctionnement/Revenus%20et%20r%C3%A9mun%C3%A9ration#r%C3%A9partition-des-revenus) chaque mois. Mais nous nous sommes rendu compte que dans certains cas notre formule de répartition ne fonctionnait pas exactement comme nous l’avions prévu. Nous sommes en train [de la rectifier](https://gitlab.com/CodeursEnLiberte/ardoise/issues/14), pour mieux prendre en compte les inégalités de revenus.

Nous nous sommes posés beaucoup de questions sur le **recrutement**. Faut-il favoriser la croissance, ou la formation d’un groupe stable ?  Quelles sont les différentes manières pour intégrer de nouvelles personnes, et contribuer à diffuser le modèle coopératif ?

Après être passés de 2 à 10 personnes en trois ans, nous avons décidé de **mettre notre recrutement direct en veilleuse** pendant quelques mois ; même si nous nous laissons la porte ouverte pour d’éventuelles cooptations. Nous voulons également **améliorer notre processus de recrutement** : s’engager sur une durée de réponse prévisible, supprimer les freins aux discussions libres entre nous, et ré-affirmer le principe d’unanimité sur les recrutements.

Nous voulons également poursuivre nos efforts d’essaimage, et de liens avec d’autres coopératives. Nous allons par exemple continuer notre **ouverture à de jeunes professionnels** (immersion, stages) – pour contribuer à la formation à notre métier, et montrer comment se vit de l’intérieur notre mouvement coopératif. Nous avons également envie de poursuivre sur la lancée de notre [intervention à Pas Sage en Seine](https://video.passageenseine.fr/videos/watch/a6a71871-7dd3-4f9e-bd6e-3893cdb46c88) cette année, et d’**aller parler de coopératives à l’extérieur**, dans des conférences ou des lieux de formation (universités, écoles, communautés d’apprentissage). Le travail lancé avec [codecode.coop](https://codecode.coop) fait aussi partie de ces efforts d’essaimage.
