+++
date = "2020-03-06T11:00:00+02:00"
title = "Éthique du développement logiciel et coopératives "
author = "Nicolas et Pierre"
categories = "Entreprise"
+++

Nous avons récemment été invités par [l’association 42l](https://42l.fr/) à parler d’éthique du développement logiciel et des coopératives.

L’intervention a été enregistrée : vous pouvez la retrouver en vidéo ci-dessous.

_Pour une version texte, vous pouvez consulter les [diapos utilisées pendant la présentation](https://hackmd.io/@n-b/codeursenliberte-42l) ; appuyez sur la touche "S" pour afficher les notes écrites des intervenants._

<iframe class="video" width="560" height="315" style="margin-top: 25px; margin-bottom: 30px;" sandbox="allow-same-origin allow-scripts" src="https://video.tedomum.net/videos/embed/fff981aa-a17a-4a8e-980e-a837042fdfcf?warningTitle=0" frameborder="0" allowfullscreen></iframe>

Par ailleurs, nous avons oublié pendant la conférence de mentionner **[codecode.coop](https://codecode.coop)**, un site d’outils, de documents et de ressources pour créer des coopératives dans l’informatique. Il contient entre autres quelques informations sur le fonctionnement interne de Codeurs en Liberté.

Merci encore à 42l pour cette invitation.
