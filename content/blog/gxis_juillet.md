+++
date = "2022-08-02T09:01:00+02:00"
title = "Ĝis : billet d’étape n°3 juillet 2022"
author = "Tristram"
categories = "cocarto"
+++

[Ĝis](https://gxis.codeursenliberte.fr) est un outil de saisie de données cartographique et territoriale. Vous lisez la troisième édition de la lettre mensuelle qui en raconte les développements, doutes, détails inutiles…

 ![Portulan de Angelino Dulcert](/blog/gxis_juillet/gxis_angelino_dulcert.jpeg)

# Que s’est-il passé en juillet ?

Canicule, pont, vacances estivales : ce mois-ci a été un peu plus calme.

Sur le plan du nom, rien n’a encore été [décidé](https://gitlab.com/CodeursEnLiberte/gxis/-/issues/20), même s’il semblerait qu’on penche vers _coocarto_.

Côté réalisations, deux fonctionalités structurantes sont arrivées.

## Gestion des droits

Parce qu’un outil de collaboration en temps réel sans droit, c’était un peu limité. Vous pouvez désormais inviter des personnes et leur assigner des droits. Très bientôt vous pourrez également publier des cartes visibles par tout le monde, même sans compte sur Ĝis.

Toutes les modifications apportées par les autres personnes sont visibles en temps réel sur votre page. Ainsi vous réduisez le risque de conflit si vous travaillez sur les mêmes objets.

## Gestion des territoires

Vous pouvez désormais créer une couche dont la donnée géographique est un _territoire_. Par exemple lorsque vous souhaitez que vos données soient associées à un département.

Nous pensons qu’une gestion éditorialisée du référentiel des territoires sera un élément fortement différenciant :

* Pour les instances auto-hébergées, les données métier de l’entreprise ou de l’administration pourront être mise à disposition, facilitant ainsi leur utilisation.
* Pour l’instance en SaaS, l’utilisateur trouvera très rapidement les données habituelles, sans devoir les chercher et importer soi-même.
* Dans les deux cas, les identifiants seront toujours les bons : finies les erreurs de saisies de code INSEE des communes et l’agacement des personnes devant utiliser la base que vous avez construite.

Une même couche peut porter sur plusieurs territoires. Par exemple, il est possible de choisir Commune et Intercommunalités.

![Exemple de Ĝis avec des données basées sur des territoires (départements)](/blog/gxis_juillet/gxis_territories.png)

# Un focus technique : base de données

[PostgreSQL](https://www.postgresql.org) est une base de données relationnelle libre réputée pour sa grande robustesse.

Nous essayons au maximum de nous appuyer dessus et en faire le moins possible au niveau du code.

## Les géographies

Les données géographiques (points, tracés et polygones dessinés par l’utilisateur) sont stockées dans des colonnes grâce à l’extension spatiale [PostGIS](https://postgis.net/) qui fait également des calculs (_bounding box_…) et des conversions (_geojson_).

## Les attributs

Les données tabulaires associées à chaque objet ressemblent furieusement à une table de base de données.

Nous avons été très tentés de faire en sorte que chaque table d’attribut soit une table postgresql. C’est par exemple ce que fait [NocoDB](https://nocodb.com/).

Cependant, cela pose de nombreuses limitations si l’on souhaite renommer une table, supprimer une colonne… En particulier le serveur _backend_ devrait systématiquement recharger le schema de la base à chaque requête, et non pas au chargement.

Nous profitons donc d’un autre aspect où PostgreSQL brille tout particulièrement : les colonnes de type [JSONB](https://www.postgresql.org/docs/current/functions-json.html).

Ainsi, les données _utilisateur_ sont stockées dans une structure en JSONB et les données techniques dans des tables « traditionnelles ».

# Cartographe du mois : l’école majorquine

Plutôt que de parler d’un cartographe en particulier, nous présentons ici toute une famille de cartographes, tout à fait adaptée pour avoir les orteils dans le sable d’une plage.

L’[école majorquine](https://fr.wikipedia.org/wiki/%C3%89cole_majorquine_de_cartographie) est connue pour ses [portulans](https://fr.wikipedia.org/wiki/Portulan) qui servaient à la navigation en indiquant les ports, les dangers potentiels et des caps à tenir.

Ce besoin de cap que les navires vont suivre (ou [Loxodromie](https://fr.wikipedia.org/wiki/Loxodromie)) rend les portulans incompatibles avec des planisphères représentants le monde entier ; du moins jusqu’à la révolution qu’a apportée Mercator, mais ce sera pour une autre édition.

Leurs portulans couvrent généralement la Méditerranée, mais vont parfois au-delà.

# Conclusion

Comme en juin, nous sommes preneurs de retours, rapports de bugs, besoins concrets et d’idées pour un nom. N’hésitez pas à nous écrire !
