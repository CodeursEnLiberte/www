+++
date = "2024-07-25T20:42:00+01:00"
title = "Notre contribution au comparateur de programmes de Nos services publics"
author = "Thomas"
categories = "Militance"
+++

Comme beaucoup, nous avons été choqué·es en voyant les résultats de l'extrême droite aux élections européennes du 9 juin 2024. Ajoutée à cela la décision du président de la République de dissoudre, il nous a fallu plusieurs jours pour sortir de notre sidération.

Dans ce contexte, j'ai eu la possibilité de contribuer au [comparateur de programmes du collectif Nos services publics](https://comparateur.nosservicespublics.fr/). En une semaine, environ 30 personnes ont participé à cet effort collectif, entre analyse fine des programmes, mise en perspectives en fonction des votes et déclarations passées, développements informatiques et conception graphique.

Avec plus de 120 000 visites entre la conférence de presse de lancement le mardi 25 juin et le second tour des élections législatives, nous considérons ces travaux comme un succès. 

![Statistiques des visites entre le lancement mardi 25 juin 2024 et le 7 juillet 2024, second tour des législatives](/blog/comparateur-nsp/01-stats.png)

> C'est très important, les victoires de les fếter, déjà parce que ça fait du bien ; et puis ensuite parce que ça s'appelle de la pédagogie des luttes : c'est parce qu'on sait qu'on peut gagner qu'on va se remobiliser la prochaine fois, et là c'était certainement pas gagné.

[Sophie Binet pour l'émission d'« À l'air libre » de Médiapart, le jeudi 11 juillet 2024 (11'31).](https://www.mediapart.fr/journal/politique/110724/sophie-binet-cgt-s-adresse-au-nfp-que-chacun-laisse-de-cote-ses-interets-partisans-et-personnels)

Partager les détails techniques de la réalisation de ce site, c'est pour moi, une célébration et je l'espère cela pourra aussi être une source d'inspiration pour d'autres.

## Une vision de départ explicite

Après un rapide échange téléphonique, j'ai eu accès à une trame, une première description du site qui avait été imaginée : [Un Google doc sur 4 pages pour chacune des parties envisagées](/blog/comparateur-nsp/01-maquette.pdf).

Un objectif simple : S'assurer que les services publics fassent partie des débâts pour ces importantes élections législatives.

9 thématiques, 31 questions concrètes et 124 réponses sourcées et structurées ont été traitées par une trentaine de personnes. C'est là l'une des forces du collectif Nos services publics : des capacités d'analyse, de sourcing et rédationnelles.

![Maquette de la page d'accueil](/blog/comparateur-nsp/02-accueil.png)
![Maquette de la page d'une thématique](/blog/comparateur-nsp/03-thematique.png)
![Maquette de la page d'une question](/blog/comparateur-nsp/04-reponses.png)
![Maquette de la page d'une réponse détaillée](/blog/comparateur-nsp/05-reponse-detaillee.png)

J'aime bien partager ces documents bruts car ils ont été notre outil de médiation pour nous lancer dans une implémentation concrète.

En parallèle des premiers développements, Thaïs et Hugo ont construit des premières maquettes.

![06-maquette-accueil.png](/blog/comparateur-nsp/06-maquette-accueil.png)
![07-maquette-thematique.png](/blog/comparateur-nsp/07-maquette-thematique.png)
![08-maquette-reponse.png](/blog/comparateur-nsp/08-maquette-reponse.png)

## La solution mise en place

Un site statique.

Pas de fioriture. Environ 130 pages HTML pré-générées ; servies par GitHub via les GitHub Pages avec un nom de domaine personnalisé pour faire sérieux.

Pas de WordPress qui nécessite une infrastucture beaucoup trop lourde et une maintenance assidue pour éviter les problèmes de sécurité.

Pas de solution commerciales, _no code_ ou _low code_. Notion a, un moment, été envisagé mais le rendu n'aurait pas pu être adapté comme ça l'a été.

Nous avons utilisé Next.js (et donc du React avec de l'outillage autour) pour construire la structure du site ainsi que la logique des pages et pour exporter le site en statique. Cet export est complètement prévu par Next.js et cela nous permet d'avoir une boîte à outils qui s'adapte à différents contextes. Les pages à construire sont définies à partir [ `getStaticPaths`](https://nextjs.org/docs/pages/building-your-application/data-fetching/get-static-paths) et `getStaticProps` permet de spéficier les données nécessaires à la construction de chaque page.

Dans notre cas, il y une page [par thématique](https://github.com/NosServicesPublics/anti-comparateur/blob/main/pages/%5Bthematique_id%5D/index.js#L24-L34) et les données nécessaires à construction de cette page sont [la thématique, les questions associées et les réponses](https://github.com/NosServicesPublics/anti-comparateur/blob/main/pages/%5Bthematique_id%5D/index.js#L36-L48).

![09-page-thematique.png](/blog/comparateur-nsp/09-page-thematique.png)

Toutes ces informations doivent bien être renseignées quelque part. Le choix a été fait d'utiliser [Grist](https://www.getgrist.com). Un logiciel open source (à la [Airtable](https://airtable.com)) qui permet d'itérer simplement sur un modèle de données relationnel. Grist est un logiciel vraiment très chouette, son équipe le décrit comme une boîte à outils pour créer des applications _low code_. Il inclut de la gestion des droits fine, une API et un puissant outil de composition de vue riches.

![10-données.png](/blog/comparateur-nsp/10-données.png)

Les contributions sont faites [sur une vue](https://nosservicespublics.getgrist.com/hXBbY6ngk5ik/Legislatives/p/3), à partir de la sélection d'une thématique qui permet l'affichage des questions associées. La sélection d'une question permet ensuite la visualisation et la saisie des différentes réponses et surtout des différentes blocs d'une réponse.

![11-grist.png](/blog/comparateur-nsp/11-grist.png)

Pour la construction du site internet, [l'API est utilisée](https://github.com/NosServicesPublics/anti-comparateur/blob/main/import.sh) pour obtenir au format JSON toutes les données nécessaires.


## Quelques détails techniques (car le diable n'est jamais loin)

### Gestion la limitation de l'API de Grist

Grist est utilisé en SaaS, c'est à dire en utilisant la version hébergée sur leurs serveurs et pas auto-hébergée par le collectif.

Il y a une limite de 5 000 appels à l'API par jour. Dans la première version du site, l'API était appelée à chaque visite. La seconde itération avec la pré-génération des pages HTML a permis de contourner cette limite et d'avoir un site plus résilient (des pages HTML simples sont faciles à déployer).

### Déploiement périodique conditionnel

À partir du moment où le site est désormais pré-généré, on se retrouve avec l'un des grands problèmes de l'informatique : le problème du cache. Une nouvelle version du site est générée lorsque des modifications techniques sont mises en ligne, c'est le principe du déploiement continu. Il fallait aussi que le site soit mis à jour lorsque les données sont mises à jour. Nous avons ajouté de la logique dans Grist, avec des dates de dernière modification et en sauvegardant l'instant du dernier déploiement. Une GitHub Action périodique a été mise en place pour déclencher un déploiement si des modifications ont été faites depuis le précédent. Cette GitHub Action a été lancée toutes les demi-heures.

### Mise en place d'une prévisualisation

Avec cette solution de déploiement périodique, les personnes qui contribuent devaient attendre une demi-heure pour voir leur contribution en ligne. C'est un délai très long pour faire des itérations. Pour permettre une visualisation immédiate nous avons mis en place une page de prévisualisation directement dans Grist. Cela repose sur une fonctionnalité très élégante de Grist, les [_custom widgets_](https://support.getgrist.com/widget-custom/). Plus précisément, Grist permet l'intégration d'_iframes_ dans ses vues avec un canal de communication sécurisé entre les deux pages. C'est une des sources de la très grande modularité et flexibilité de Grist.

La page dédiée à la prévisualisation fonctionne comme la page d'affichage normal d'une réponse mais [sa logique fait l'hypothèse](https://github.com/NosServicesPublics/anti-comparateur/blob/main/pages/preview.js#L10-L52) d'être intégrée à Grist via une iframe et d'accéder directement à des données.

### Prise en compte du texte formaté

Enfin, pour terminer cet article je voulais présenter la solution mise en place pour la gestion du texte formaté. En effet, l'une des forces du collectif Nos services publics est sa capacité d'analyse et de faire le lien avec les données sous-jacentes, fiables. Le texte formaté permet de mettre en avant avec du **gras** et de mettre des liens vers des ressources externes. Il n'était pas envisageable de demander aux contributeurises de saisir du texte en HTML. Il fallait aussi que les personnes puissent faire des copiers-collers depuis leurs différents outils dans lequel elles avaient fait le travail rédactionnel.

Une fois de plus Grist vient à notre rescousse avec, là encore, un _custom widget_ directement mis à disposition par Grist (et sa communauté). En effet, il s'agit du [_Notepad widget_](https://support.getgrist.com/widget-custom/#notepad) qui permet exactement de modifier du texte formaté avec une interface classique (basée sur [Quilljs](https://quilljs.com/)). Cela étant dit, le format utilisé  par Quill (et donc Grist) pour modéliser du texte formatté n'est pas de l'HTML.

Étant donné que Grist est un logiciel open source, [j'ai pu proposer une amélioration](https://github.com/gristlabs/grist-widget/pull/129) de ce _Notepad widget_ qui permet d'exporter dans Grist l'équivalent HTML qui est bien plus facile à intégrer ailleurs.

Nous avons temporairement utilisé la version corrigée de mon côté. En 3 jours, mon amélioration a été intégrée et pouvait déjà être réutilisée par d'autres personnes de la communauté de Grist. Cette amélioration a ensuite été [mise en avant dans la newsletter mensuelle de juine 2024](https://community.getgrist.com/t/june-2024-newsletter-research-templates-running-rootless-and-community-contributions/5482
) de Grist avec une démonstration.

Voilà comment en moins d'une semaine, [ce comparateur](https://comparateur.nosservicespublics.fr/) aura donc pu voir le jour. 🎉

Pour terminer, je partage une reflexion partagée lors de notre rétrospective, organisée la semaine suivant le second tour des élections. Nous sommes nombreuses et nombreux à avoir souligné la situation d'urgence absolue, une urgence qui a été un moteur pour nos travaux et notre implication. Cette urgence reflète aussi une situation exceptionnelle, non durable et difficilement réplicable.

Pour moi, cela renforce la pertinence de la mise en place d'équipes responsables et autonomes telles qu'elles ont existé au sein de beta.gouv.fr et dans certains collectifs militants comme ceux de Nos retraites et de Nos services publics.
