+++
date = "2023-02-06T15:46:00+01:00"
title = "cocarto : billet d’étape n°6 (janvier 2023)"
author = "Tristram"
categories = "cocarto"
+++

[cocarto](https://cocarto.com) est un outil de saisie de données cartographiques et territoriales. Vous lisez la sixième édition de la lettre mensuelle qui en raconte les développements, doutes, détails inutiles…

![Photo de la dalle de Saint Bélec](/blog/cocarto_janvier/dalle_st_bélec.jpeg)

# Ça devient sérieux : le juridique entre en jeu

cocarto se divise en deux :

* le logiciel, dont le [code source](https://gitlab.com/CodeursEnLiberte/cocarto/) est sous licence libre [AGPL](https://www.gnu.org/licenses/agpl-3.0.html)
* un service que nous hébergeons https://cocarto.com/

Pour ce dernier, en tant qu’hébergeur, nous avons des obligations (notamment de protection des données personnelles, RGPD), mais nous pouvons aussi imposer nos conditions d’utilisation.

Sur https://cocarto.com/legal nous avons mis les trois textes en relation avec l’hébergement : mentions légales, protection des données et conditions générales d’utilisation.

Nous avons voulu faire en sorte que le texte soit compréhensible, avec des liens vers les documents de référence pour mieux contextualiser et expliquer pourquoi lorsqu’on fait les choses correctement, il n’y pas besoin de bannière de cookies.

C’est également une étape importante pour nous : elle ancre notre souhait de gagner de l’argent avec une offre hébergée de cocarto.

# Côté technique

Nous avons travaillé sur des éléments peu visibles

* fiabilité : soucis de connexion avec redis réglés, amélioration la surveillance de la production,
* performances : chasse aux goulets d’étranglement côté _front-end_ et mise en place de cache en _back-end_,
* poussage de pixels : des marges améliorées, des icônes replacées et des interactions explicitées.

# La plus ancienne carte connue

Découverte il y a plus d’un siècle, la [dalle gravée de Saint-Bélec](https://fr.wikipedia.org/wiki/Dalle_grav%C3%A9e_de_Saint-B%C3%A9lec) est considérée comme la plus ancienne carte connue. Elle aurait 4000 ans et pèse plus d’une tonne.

![Interprétation de la dalle de Saint Bélec](/blog/cocarto_janvier/carte_st_bélec.jpeg)

Ce n’est qu’en 2021 qu’[une équipe de recherche](https://hal.science/hal-03507764) a démontré que la dalle représentait une zone géographique d’environ 30×20 km dans le Finistère. On y retrouve, de façon symbolisée et simplifiée, les éléments importants pour les _cartographes préhistoriques_ qui l’ont réalisée : le relief et les cours d’eau, autour de l’actuelle vallée de l’Odet, ainsi que l’emplacement de structures : enceinte, tumulus, et route.

# Conclusion

Notre ressenti est d’aller vers un produit de plus en plus solide : que ce soit techniquement, esthétiquement et même juridiquement ! La suite va consister à continuer à échanger avec des utilisateurs (potentiels) et résister à l’envie de rajouter plus de fonctionnalités.

Vous voulez faire partie des bêta-testeurs et bêta-testeuses qui donneront le cap au produit ? Contactez-nous : bonjour@cocarto.com
