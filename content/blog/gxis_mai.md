+++
date = "2022-06-01T10:49:00+02:00"
title = "Ĝis : billet d’étape n°1 mai 2022"
author = "Tristram"
categories = "cocarto"
+++

[Ĝis](https://gxis.codeursenliberte.fr) est un produit initié par Codeurs en Liberté.
Ce billet est le premier d’une série que nous espérons mensuelle.

Il s’agit de raconter comment se passent le développement, les avancées, les choix techniques, les difficultés, les applications…

![Carte du monde de Al-Idrīsī](/blog/gxis_mai/TabulaRogeriana.jpg)
## Qu’est-ce donc ?

Nous n’avons pas encore de _punchline_ qui marque les esprits.

C’est un outil web pour de la _saisie_ de données à caractère géographique ou territorial ; tout ça de manière collaborative et sans avoir d’expérience de cartographe.

Quelques exemples :

* Pour le loisir, planifier sa randonnée : points pour dormir, tracés à parcourir ;
* En tant que petite commune, numériser les arrêtés qui interdisent aux poids lourds de se coincer sous un pont ;
* Pour une association militante, co-construire des propositions de voies express vélo ;
* Pour une ONG, associer des données de travail à sous-divisions administratives.

Par contre ce ne sera pas un outil pour faire des très belles cartes, pour saisir de la donnée dans [OpenStreetMap](https://openstreetmap.org),
ni pour utilisateur experts.

## La genèse

En échangeant avec des amis, des collègues et des clients, nous nous sommes convaincus qu’il y avait un réel besoin d’un outil simple « pour faire une carte ».
Il se trouve que c’est dans nos cordes. Par ailleurs, nous avions chez Codeurs en Liberté envie de travailler ensemble sur un produit, et pas simplement facturer des journées, chacun dans son coin.

Après avoir tourné autour du pot pendant six mois,
lors de notre [assemblée générale de 2022](/blog/ag_2021) nous avons décidé de ré-investir une partie de nos bénéfices dans une première version du produit.

En dehors de quelques tests techniques, le développement a commencé en douceur en mai et devrait s’accélérer en juin.

## Le nom

Il s’agit d’un nom temporaire, jusqu’à trouver l’inspiration ; pour l’instant les pistes sont _geotable_ et _co-carto_.

_Ĝis_ vient de l’esperanto : version courte de _ĝis baldaŭ_ qui veut dire _à bientôt_. Mais c’est également un jeu de mots avec
[GIS, geographic information system](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27information_g%C3%A9ographique) qui est utilisé dans
des logiciels bureau comme [QGIS](https://www.qgis.org) ou [ArcGIS](https://www.arcgis.com).

## Les premiers développements

Ce premier mois a servi à mettre en œuvre les fondations indispensables :

* internationalisation,
* système de compte utilisateur,
* charte graphique…

Mais aussi des points plus spécifiques :

* Importer un référentiel territorial (ensemble des communes) ;
* Comment représenter dans PostgreSQL des géométries hétérogènes.

## Un focus technique

Nous sommes partis sur du [Ruby on Rails 7](https://rubyonrails.org/2021/12/15/Rails-7-fulfilling-a-vision).

En profitant des progrès apportés par le HTTP/2, le support du Javascript moderne par les navigateurs, nous pouvons faire une application monolithique. Il n’y a plus une une application _backend_ et une autre _frontend_.

Les pages html sont générés par le serveur, comme en 1999 en php chez Multimania. Il n’y a plus de dépendance à node, ni de transpilation ; un monde de simplicité et réplicabilité.

Les librairies javascript (comme [Maplibre](https://maplibre.org) pour l’affichage de la carte) sont importée directement sans passer par une moulinette.
[Hotwire](https://hotwire.dev/) permet d’avoir un comportement aussi fluide qu’avec une [Single-page App](https://fr.wikipedia.org/wiki/Application_web_monopage), sans les inconveniants de cohérence à maintenir entre _front_ et _back_.

## Cartographe du mois : Al-Idrīsī

[Al-Idrīsī](https://fr.wikipedia.org/wiki/Al_Idrissi) était un cartographe du 12ème siècle.

Il est connu pour les cartes du [Livre de Roger](https://fr.wikipedia.org/wiki/Tabula_Rogeriana)
qui aura été pendant plusieurs siècles la représentation du monde la plus précise et qui est l’illustration en tête.

D’ailleurs c’est le bon moment pour rappeler que mettre le nord en haut n’est qu’une construction sociale.

## Conclusion

Nous avons encore beaucoup à raconter : sur la technique, sur notre vision. Rendez-vous en juin.

N’hésitez-pas à nous répondre, nous avons hâte de connaitre vos retours et vos attentes.

Et si vous préférez recevoir ce contenu par email, inscrivez-vous [à notre infolettre](https://buttondown.email/gxis).
