+++
date = "2025-02-01T17:24:32+01:00"
title = "Un pourcent pour le libre"
author = "Tristram"
categories = "Entreprise"
+++

Ce qui fait la force de l’informatique, c’est que nous pouvons réutiliser le travail de nos camarades sans les priver quoi que ce soit. S’il y a une liberté de réutilisation et de rediffusion de ce travail, alors nous pouvons nous appuyer dessus pour créer de nouvelles choses, au lieu de refaire ce qui a déjà été fait. Et à notre tour mettre à disposition des briques pour les suivant·es. Le logiciel libre permet cela, et nous nous en servons massivement : notre navigateur web, nos bibliothèques, nos outils de travail sont (généralement) des logiciels libres. Comment alors contribuer à ce logiciel libre, pour que d’autres personnes profitent à leur tour de ce que nous faisons ?

Cette question revient très souvent et nous avons déjà raconté des choses que nous avons expérimentés ([contribuer au libre](contribuer_au_libre), [contribuer aux communs](comment_contribuer_aux_communs)) mais il faut se rendre à l’évidence : nous n’avons pas pu contribuer autant que nous profitons du logiciel libre. Des initiatives comme [copie publique](https://copiepublique.fr/) nous poussent à contribuer financièrement. Ça tombe bien, nous sommes une entreprise à but lucratif, nous avons de l’argent que nous pouvons dépenser.

## Nos règles

Lors de l’[assemblée générale de 2024](https://gitlab.com/CodeursEnLiberte/fondations/-/blob/main/comptes%20rendus/2024-07-12%20AG%20ordinaire/compte%20rendu.md#r%C3%A9solution-9-attribution-de-1-du-ca-2023-au-financement-de-lopen-source), nous avons décidé de consacrer 1% de notre chiffre d’affaire à financer du logiciel libre. Idéalement nous souhaitons financer des outils que nous utilisons dans notre travail quotidien et qui ont besoin de soutien financier. Pendant une longue période ouverte nous avons essayé d’identifier nos dépendances ([github peut aider](https://github.com/sponsors/explore)). Certains contributeurs importants comme [David Tolnay](https://github.com/sponsors/dtolnay) nous invitent à sponsoriser d’autres personnes, démarche que nous avons apprécié et nous a aidé.

Finalement nous avons réparti 7 000 € de la manière suivante :
- 2 × 1 000 € de dons direct en une seule fois,
- le reste sur 6 développeur·euses sur [github](https://github.com/orgs/CodeursenLiberte/sponsoring) en versement mensuel sur une année.

On nous a soufflé que les dons mensuels étaient souvent préférés pour avoir une certaines visibilité sur les revenus futurs.

## Des interrogations

Même, si nous étions d’accord sur le principe général, il reste des doutes auxquelles nous n’avons pas de réponse claire.

Faire les robins des bois en facturant plus ne fait pas l’unanimité au sein de notre coopérative ; mais c’est un débat sur nos visions du monde qui dépasse largement le besoin de financement du logiciel libre.

Est-il pertinent de donner sur github qui appartient à Microsoft et qui prend des frais importants de gestion (3%, en plus des 40€ pris par les banques pour faire un virement vers les États-Unis) ?

Nous n’avons pas de réponse tranchée, [à vous d’y réfléchir](https://gitlab.com/CodeursEnLiberte/fondations/-/issues/215).

## Pourquoi en parler

Mais pourquoi est-ce que nous en parlons ? Pour nous faire mousser, bien sûr ! mais aussi pour essayer d’entrainer d’autres entreprises dans ce genre d’initiatives.

Est-ce que votre employeur finance les outils que vous utilisez ? Parlez-en pour que ça devienne une évidence !

## Conclusion

Il est nécessaire de multiplier les actions pour généraliser le logiciel libre :
- contribuer aux logiciels que nous utilisons,
- maintenir des bibliothèques libres qui ont des réutilisateurs,
- financer des devs pour leur permettre d’être indépendant·es,
- faire du prosélytisme.

Ça ne suffira pas à atteindre une société basée sur l’entraide mutuelle, mais c’est un pas de plus vers une société plus souhaitable et cela concerne notre métier et notre travail quotidien. Nous avons donc l’obligation de nous interroger plus sur notre place dans cet écosystème.
