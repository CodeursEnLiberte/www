+++
date = "2023-11-15T18:00:00+01:00"
title = "cocarto : billet d’étape n°11"
author = "Nicolas"
categories = "cocarto"
+++

[cocarto](https://cocarto.com/) est un tableur de données géographiques[^1]. Vous lisez la onzième édition de la [lettre mensuelle](https://buttondown.email/cocarto) qui en raconte les développements, doutes, détails inutiles…

## Infobulle

> **Étymologie:** (XXe siècle) Mot-valise d’information (apocopé et préfixé en info-) et bulle, à cause de sa proximité avec les bulles (phylactères) de bandes dessinées. ([wiktionnaire](https://fr.wiktionary.org/wiki/infobulle))

Curieusement, on avait une certaine aversion à développer cette fonctionnalité : cocarto est un outil de gestion de données cartographiques, avec une carte et un tableur. Les données géographiques vont sur la carte, et les données attributaires vont dans le tableur. Pas de mélange, pas de rendu conditionnel[^2], pas de [sémiologie graphique](https://fr.wikipedia.org/wiki/Sémiologie_graphique).

Cependant, c’était demandé par pas mal d’utilisateurs, parce qu’à l’usage, ça semble évident: il s’agit d’identifier les objets sur la carte. Jusqu’ici, quand on cliquait sur un point sur la carte, la rangée correspondante était sélectionnée dans la table. Maintenant, ça fait toujours ça, mais en plus :

![Al Gladiatore](/blog/cocarto_11/infobulle.png)

On a fait pour le moment la *fonctionnalité viable minimale*, comme disent les jeunes : la texte de la première colonne est utilisée pour le contenu de l’infobulle. À venir, [le choix de la colonne](https://gitlab.com/CodeursEnLiberte/cocarto/-/issues/351) et [l’affichage des photos dans l’infobulle](https://gitlab.com/CodeursEnLiberte/cocarto/-/issues/352).

## Pièces à joindre

À propos de photos, on a (enfin!) terminé une fonctionnalité qui était *beta* depuis un bon moment : la gestion des photos et des pièces jointes. Désormais, vous pouvez ajouter des photos à un rang, les supprimer, les réajouter, il y une barre de progression pendant le téléversement, et on affiche même les métadonnées comme la taille des images et la durée des videos. Hop hop hop.

![Pizza](/blog/cocarto_11/photos.png)

## Libre marché (mais pas gratuit)

On en parlait [le mois dernier](http://www.codeursenliberte.fr/blog/cocarto_octobre/), nous avons commencé à faire de vrais efforts *marketing*. (Comprendre : on a un tableau de prospects et de gens contactés / à relancer / intéressés.) Plus concrètement, [cocarto a maintenant un tarif public](https://cocarto.com/presentation#combien-ca-coute) :

![Tarifs](/blog/cocarto_11/tarifs.png)

Les usages raisonnablement petits sont gratuits, l’abonnement à 99€ vous donne accès au support et à certaines fonctionnalités, et sur devis, nous proposons le développement de fonctionnalités spécifiques et accompagnons votre projet de bout en bout. Enfin, cocarto est un logiciel libre et auto-hébergeable, donc rien ne vous empêche de le déployer chez vous. [Parlez-nous en](mailto:bonjour@cocarto.com), c’est ça qui nous intéresse.

Par ailleurs, et sans trop divulgacher, on devrait pouvoir d’ici peu vous parler d’un projet, réalisé avec cocarto, et qui devrait avoir un peu de visibilité. Confettis !

## Jointures par les deux bouts

Je l’avoue, le terme « jointure » me pose problème en tant que développeur d’une appli de cartographie. Dans le domaine des bases de données, il a un sens assez précis,  quand on [parle de SQL](https://en.wikipedia.org/wiki/Join_(SQL)). Des générations d’étudiants ont passé des nuits blanches sur la différence entre un `INNER JOIN` et un `LEFT OUTER JOIN` .

Les géomaticiens parlent aussi de jointure, pour la même notion, mais de façon beaucoup plus concrète. La définition se résume généralement à :
> To join a table with this shapefile, we need a unique and common attribute for each feature.[^3]

Bref, il s’agit de prendre deux listes de *trucs*, et de les réunir par un attribut commun, un *identifiant*. Par exemple, des départements, avec d’un côté leur [contour géographique](https://adresse.data.gouv.fr/data/contours-administratifs/2023/geojson), et de l’autre la [population](https://www.insee.fr/fr/statistiques/6683035?sommaire=6683037). Pour mettre ces données en commun, on va faire correspondre leur numéro de département :

<figure>
  <img src="jointure.png" alt="jointure">
  <figcaption><code>CODDEP</code> dans les données de population correspond à <code>code</code> dans la géographie.</figcaption>
</figure>

Et voilà ce que ça donne dans cocarto :

![population](/blog/cocarto_11/population.png)

En fait, l’import de données attributaires est sensiblement la même chose que le *réimport* de données géographiques qui évoluent. On continue d’ailleurs d’avancer sur ce sujet : un des chantiers en cours est le réimport quotidien de données à partir d’un flux WFS, pour garantir des données toujours fraiches [^4].

## La plus petite fac de France

En parlant de géomaticiens, voici l’occasion de parler d’une formation que j’ai suivie l’an dernier, et pour laquelle il reste des places cette année. Le [DU C-SIGAD](https://iuga.univ-grenoble-alpes.fr/formations/diplome-d-universite/), pour “Cartographie, systèmes d'information géographique et analyse de données”, qui se passe au [campus rural du CERMOSEM](https://iuga.univ-grenoble-alpes.fr/institut/le-cermosem/le-campus-rural-le-cermosem-275906.kjsp), à Mirabel, en Ardèche, dans un cadre reposant loin du stress de l’urbanité :

<figure>
  <a href="https://iuga.univ-grenoble-alpes.fr/institut/le-cermosem/formations-du-cermosem/">
    <img src="mirabel.jpeg" alt="mirabel">
  </a>
  <figcaption>Le campus <a href="https://cocarto.com/fr/share/Jr-wbkdeZctCM6iF">se trouve dans le cadre</a> de la photo.</figcaption>
</figure>

C’est à destination de toutes les personnes qui se retrouvent à faire des cartes *sans que ça soit leur premier métier*, ce qui est un point commun avec cocarto. C’est à la fois dense, solide sur la théorie, et très pratique. Je recommande, ★★★★★ would eat here again.

## 

C’est tout pour cette fois-ci ! Merci encore de nous avoir lus. N’hésitez pas à nous envoyer vos commentaires sur [bonjour@cocarto.com](mailto:bonjour@cocarto.com), à regarder la [roadmap](https://gitlab.com/CodeursEnLiberte/cocarto/-/milestones/19), et à nous signaler des bugs.

[^1]: Tiens, notre définition évolue ! Avant, c’était un *outil de saisie de données cartographiques et territoriales*, et au tout début, *un produit initié par Codeurs en Liberté*. Peut-être que ça va encore changer.
[^2]: Même si on en parle régulièrement dans l’équipe, ne jamais dire jamais.
[^3]: Ici par exemple : https://www.qgistutorials.com/en/docs/performing_table_joins.html
[^4]: Aussi appelées données chaudes, ce qui admettons-le peut être source de confusion.