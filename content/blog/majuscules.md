+++
date = "2022-03-17T18:19:00+01:00"
title = "Les majuscules, c’est compliqué"
author = "Thibaut"
categories = "Design"
+++

![](/blog/majuscules/majuscules-base-de-donnees.jpg)

Lorsque notre activité consiste à manipuler de l'écrit à l'écran, l'utilisation soutenue des majuscules peut affecter notre confort de travail. Voici une explication rapide.

## Pourquoi est-ce qu’on utilise des majuscules pour écrire certains groupes de mots, voire des phrases entières ?
	
C'est souvent par habitude ou conventions héritées de collègues, ou suite à des expériences passées que nous utilisons des majuscules pour formater certains textes. Parfois, c'est pour faire comprendre que ce qui est écrit est vraiment très très très important, ou revêt un caractère officiel.

## Pourquoi il faudrait éviter l’excès de majuscules ?

En résumé, le cerveau doit fournir plus d'efforts pour les interpréter au delà de l'équivalent d'une phrase. Donc à la fin d'une journée à travailler de la matière texte en majuscules, nous sommes plus fatigué(e)s que si elle était composée de minuscules. Pour comprendre pourquoi, nous allons voir comment fonctionne la lecture d'un texte, et faire un test pour faire l'expérience de la différence entre minuscules et majuscules à l'échelle d'une liste de mots.

### La progression du regard dans un texte

Contrairement à la fluidité qu'on ressent en lisant, que vous ressentez sans doutes à l'instant même, le regard progresse par à-coups dans les lignes en faisant des sauts d'un groupe de lettres à l'autre. Recherchez les termes « [eye tracking text](https://duckduckgo.com/?q=eye+tracking+text&iar=videos&iax=videos&ia=videos) » sur la plateforme vidéo de votre choix et vous pourrez en voir la démonstration. L'efficacité de la reconnaissance des mots en coups d'œil est donc déterminante pour la vitesse et le confort de lecture.

### Nous reconnaissons les mots comme nous reconnaissons un visage

Il y a encore un peu de controverse sur la façon précise dont on reconnait les mots, mais pour résumer sans fâcher personne disons que nous utilisons l'habitude et la mémoire pour reconnaitre un ensemble de lettres comme étant un mot en particulier. On a longtemps pensé que le dessin global d'un mot écrit primait sur la lecture des lettres qui le composent, mais des études remettent ce fonctionnement en question. Il s'avère en tous cas que ces mots-visages en majuscules sont mieux reconnus que ceux en minuscules lorsqu'ils sont seuls et isolés. Mais l'inverse est vrai quand les mots sont regroupés sur une longue ligne ou en paragraphes.

Les lettres sont plus distinctes les unes des autres dans leur dessin minuscule que dans leur forme de majuscule ; elles offrent plus d'indices pour qu'on les reconnaisse. C'est un phénomène qui peut jouer lorsqu'on progresse dans un texte.

![](/blog/majuscules/majuscules-1-a-g-bpr.png)

### Nous utilisons la moitié supérieure des mots plus que leur moitié inférieure pour les reconnaitre

C'est le même principe d'indice qui existe entre les majuscules et les minuscules qui est à l'œuvre ici. Le dessin du haut des lettres est plus varié que le bas, nous permet de mieux identifier la lettre, et par extension le mot.

![](/blog/majuscules/majuscules-2-informatique.png)

Ce phénomène est vrai pour les majuscules comme pour les minuscules.

![](/blog/majuscules/majuscules-3-lecture.png)

Mais il apporte plus d'utilité dans le cas des minuscules.

![](/blog/majuscules/majuscules-4-tricycle.png)

C'est pour ces raisons que nous préférons lire les textes en minuscules plutôt qu'en majuscules. Ce n'est pas pour rien que les maisons d'éditions ne publient pas de romans en capitales.

### Ce qui est vrai pour les textes l’est aussi pour les listes

Et c'est là où nous revenons au sujet de nos écrans de travail, le cas du tableur étant le plus commun.

Faisons un petit exercice : chronométrez le temps qu'il vous faut pour reconnaitre l'ensemble des mots dans des deux listes. La première contient des mots en minuscules et la deuxième en majuscules.


<div style="border: 1px solid #ddd; margin: 2em 0">
	<a href="" onclick="this.children[0].children[0].style.visibility = 'visible'; return false;">
		<div style="background-color: #f6f6f6">
			<img style="visibility: hidden" src="/blog/majuscules/majuscules-5-mots-miniscules.png"/>
		</div>
		<div style="text-align: center; margin: 1em 0">Afficher l’image</div>
	</a>
</div>

<div style="border: 1px solid #ddd; margin: 2em 0">
	<a href="" onclick="this.children[0].children[0].style.visibility = 'visible'; return false;">
		<div style="background-color: #f6f6f6">
			<img style="visibility: hidden" src="/blog/majuscules/majuscules-6-mots-majuscules.png"/>
		</div>
		<div style="text-align: center; margin: 1em 0">Afficher l’image</div>
	</a>
</div>

Voici les temps que nous avons observés entre nous. Il y a de fortes chance que vous constatiez chez vous aussi ce même écart.

| minuscules | majuscules |
|:--:|:--:|
| 9,5 s | 34,5 s |
| 8,5 s | 15 s |
| 7,6 s | 13,5 s |
| 18 s | 36 s |
| 11 s | 33 s |

## L’un dans l’autre

Nous avons constaté que les majuscules ne facilitent pas le confort de lecture de groupes de mots. Lorsque notre métier nous amène à consulter et manipuler beaucoup d'écrit, on ne se facilite pas la tâche en utilisant de façon insistante des majuscules, que ce soit pour des phrases ou pour de longues listes. Même si c'est par habitude, ou parce que c'est très important. Pour les listes d'acronymes en revanche, pas de salut.

Une note finale pour nos collègues développeurs. Une bonne pratique à adopter est de ne pas utiliser de majuscules dans les données enregistrées en base. Si un service demande à ce que les noms de famille soient affichés en majuscules, il vaut mieux avoir `Dupont` en base pour avoir le choix de l'afficher comme « DUPONT » pour ce service, puis dans un autre contexte pouvoir l'afficher comme  « Dupont » avec quelques règles de présentation.

UN GRAND MERCI.