+++
date = "2023-04-07T17:00:00+02:00"
title = "cocarto : billet d’étape n°8 (mars 2023)"
author = "Nicolas"
categories = "cocarto"
+++

[cocarto](https://cocarto.com) est un outil de saisie de données cartographiques et territoriales. Vous lisez la huitième édition de la [lettre mensuelle](https://buttondown.email/cocarto) qui en raconte les développements, doutes, détails inutiles…

![Connexion géodésique des observatoires de Greenwich et de Paris.](/blog/cocarto_mars/Anglo-French_survey_of_1784-1790.jpg)

# Import / Export

L’échange de données est évidemment une fonctionnalité centrale de cocarto : après tout, c’est un outil de gestion de données. En fait, une des motivations à l’origine de cocarto était d’aider les gens qui aujourd’hui s’envoient des fichiers csv par email. Nous avons depuis le début une forme d’export en geojson. Cela permet notamment d’utiliser les données d’une couche cocarto dans un SIG comme QGIS, en spécifiant comme source de données l’url du geojson. L’affichage sur la carte dans cocarto-même se base jusque-là sur du geojson — mais je m’avance.

Nous nous attaquons maintenant à ce chantier de façon frontale : les données de cocarto peuvent être exportées en csv et en geojson, et être importées depuis un fichier csv. Il reste à faire [tout le reste](https://gitlab.com/CodeursEnLiberte/cocarto/-/issues/49):
- l’import et l’export vers des fichiers Excel, LibreOffice, peut-être aussi Google spreadsheet, et des formats géographiques : Shapefile et geopackage,
- la correspondance entre les colonnes du fichier et les champs de la couche,
- le ré-import avec mise à jour des rangs existants,
- l’export global d’une carte avec toutes les couches, et les pièces jointes,
- l’automatisation de tout ça,
- les performances,
- et… la gestion d’erreurs.

C’est déjà utilisable aujourd’hui! Pour résumer, un fichier csv tel qu’il est exporté peut-être réimporté : tant que les noms des colonnes correspondent exactement aux noms des champs de la couche, ça devrait marcher ! N’hésitez pas à nous dire ce qui fonctionne, et surtout ce qui ne fonctionne pas.

# Tuiles

Parlons un peu technique : cocarto affiche les mêmes données sur une table et une carte, en répercutant les modifications de l’une à l’autre. Dans ce que nous avons fait jusque là, c’est la table qui est la « source de vérité » pour la carte: les modifications sur la cartes créent directement un nouveau rang sur la table (un `<tr>` dans la `<table>`), qui est envoyé au serveur comme un formulaire web. Dans l’autre sens, quand un nouveau rang est ajouté dans la table depuis le serveur (via une `websocket`), le code ajoute aussi une `Feature` à la carte.Concrètement, la géométrie est dans champ caché `row_geojson` du rang. Ça marche assez bien: il n’y a qu’un seul canal de communication entre le client et le serveur, et ça facilite la synchronisation entre plusieurs utilisateurs. 

Par contre, ça ne peut pas fonctionner avec des gros volumes de données. _Ça ne passe pas à l'échelle_.
- En général, c’est assez mal vu d’avoir des tables html de plusieurs milliers d’éléments ; si en plus il faut décoder le geojson de chaque rang individuellement, ça ne fonctionne plus.
- Ça empêche aussi de paginer, de filtrer, et de trier la table librement: si un rang n’est pas dans le html, il n’est pas non plus sur la carte.
- Enfin, ça empêche de [simplifier](https://fr.wikipedia.org/wiki/Généralisation_cartographique) la géométrie des lignes et des polygones selon le zoom: par exemple, les lignes de littoral n’ont pas besoin d’être détaillées quand on affiche la terre entière.

Nous avons donc commencé à envoyer le contenu des cartes en tuiles vectorielles [MVT](https://docs.mapbox.com/data/tilesets/guides/vector-tiles-standards/). C’était [déjà le cas](/blog/gxis_juin/) du fond de carte: ça devrait nous offrir plus de souplesse et de performance. Il reste beaucoup à faire, notamment sur la gestion de l’ajout d’un nouveau rang: est-ce qu’on recharge les tuiles concernées ? Est-ce que le serveur envoie, comme aujourd’hui, le geojson de ce rang en particulier ? [Réponse dans un prochain numéro](https://gitlab.com/CodeursEnLiberte/cocarto/-/merge_requests/488).

# Méridien de Paris

Nous avons déjà mentionné la bibliothèque [MapLibre](http://maplibre.org), que nous utilisons pour l’affichage de la carte sur cocarto ; c’est notamment ça qui nous permet d’utiliser des tuiles vectorielles.

MapLibre utilise la projection [Web Mercator](https://en.wikipedia.org/wiki/Web_Mercator_projection) [^1], le standard de fait d’à peu près toutes les cartes sur internet. Entre autre conséquences, cela signifie que le système de référence est WGS84, et donc que le méridien zéro est à Greenwich[^2]. En France, on a utilisé le méridien de Paris jusqu’à la fin du XIXe siècle et la [conférence de Washington](https://fr.wikipedia.org/wiki/Conférence_internationale_de_Washington_de_1884). C’est sur ce méridien de référence qu’ont été établi nombre de travaux cartographiques, notamment la triangulation de la France (entre Dunkerque et le Pic du Canigou), ou la définition historique du mètre: la dix-millionième partie de la méridienne passant par Paris et reliant le pôle Nord à l'Équateur. Aujourd’hui, on a l’habitude de dire que Paris est à 2 degrés et quelques de longitude Est ; c’est un détail à prendre en compte quand on recherche un trésor de pirate.

![tintin](/blog/cocarto_mars/rackham_le_rouge.jpg)

C’est tout pour ce mois-ci ! N’hésitez pas à [tester cocarto](https://cocarto.com/fr/users/sign_up), nous dire ce que vous en pensez, ce qui vous manque, ce qui vous agace et comment vous pourriez l’utiliser. Contactez-nous : bonjour@cocarto.com


[^1]: [Il est question](https://github.com/maplibre/maplibre-rs/issues/139) de pouvoir utiliser d’autres projections, mais ce n’est pas pour tout de suite.
[^2]: À 102 m près. [C’est compliqué.](https://www.unoosa.org/pdf/icg/2015/icg10/wg/wgd04.pdf)
