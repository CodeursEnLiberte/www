+++
date = "2023-12-13T15:00:00+01:00"
title = "cocarto : billet d’étape n°12"
author = "Tristram"
categories = "cocarto"
+++

[cocarto](https://cocarto.com/) est un tableur de données géographiques. Vous lisez la douzième édition de la [lettre mensuelle](https://buttondown.email/cocarto) qui en raconte les développements, doutes, détails inutiles…

## Le cas concret d’un client payant

[![Capture d’écran de l’observatoire du réseau vélo Île-de-France](https://observatoire-vif.velo-iledefrance.fr/preview.jpg)](https://observatoire-vif.velo-iledefrance.fr)

Dans la [lettre précédente](https://codeursenliberte.fr/blog/cocarto_11/), nous vous avions promis de parler d’un projet concret réalisé avec cocarto. Il s’agit de l’[Observatoire du Réseau Vélo Île-de-France](https://observatoire-vif.velo-iledefrance.fr).

Le [collectif vélo Île-de-France](https://velo-iledefrance.fr/), qui regroupe 42 associations cyclistes, a confié à _Codeureuses en Liberté_ la réalisation de l’observatoire afin de mieux communiquer sur les points de blocage et les avancées du [Réseau vélo Île-de-France (VIF)](https://www.iledefrance.fr/toutes-les-actualites/reseau-velo-ile-de-france-vif-plus-de-pistes-cyclables-pour-les-deplacements-quotidiens). Nous avons donc développé deux choses distinctes :
- des nouvelles fonctionnalités sur cocarto, notamment sur l’import de données;
- le site public de l’observatoire à proprement parler.

Concrètement, les géomaticiens de la région mettent à disposition un flux [WFS](https://fr.wikipedia.org/wiki/Web_Feature_Service) avec les aménagements du réseau VIF. Ces données sont importées quotidiennement dans cocarto et les membres du collectif complètent les données, en particulier pour indiquer les causes de blocage.
La gestion fine des permissions permet un accès « back-office » restreint à certaines personnes pour consulter des données de travail, non publiées.

Les données du document cocarto sont alors réexportées en [GeoJSON](https://fr.wikipedia.org/wiki/GeoJSON) afin d’alimenter le site public de l’observatoire.

Les fonctionnalités d’import et d’export sont donc essentielles pour ce projet, tout particulièrement le ré-import quotidien pour avoir les données à jour sans pour autant perdre l’enrichissement par le collectif. Nous sommes particulièrement fiers d’avoir participé à ce projet, qui en plus d’être un très bon cas d’usage de cocarto, est un site d’utilité publique.

## Trier ses documents

![Capture d’écran de cocarto indiquant comme trier les colonnes](/blog/cocarto_12/tri_tableau.webp)

Cocarto c’est à 50 % un tableau de données. Tout naturellement, une demande qui revient souvent est de pouvoir trier et filtrer les données.

[C’est fait](https://gitlab.com/CodeursEnLiberte/cocarto/-/merge_requests/645). Et le filtrage devrait suivre assez naturellement.

Cette fonctionnalité qui semble évidente a pourtant tiré son lot de questions :

- est-ce que le tri s’applique sur la vue courante ou pour tout le monde (que la vue courante)
- comment décide-ton du sens de tri ? (dans le menu déroulant de configuration de la colonne ; nous n’en sommes pas satisfaits et cela évolura)
- quelle forme aura le petit triangle indiquant le tri en cours ? (à trois pointes)
- le tri est-il fait par le _front_ ou par le _back_  ? (le back)
- veut-on permettre de trier selon plusieurs colonnes ? (pas pour l’instant)
- l’opercule du pot de yaourt va-t-il dans la poubelle jaune ? (oui)

Se retrouver dans une carte avec de nombreuses lignes donc un peu plus facile et nous allons continuer à faciliter l’édition des données du tableau.

## Inō Tadataka

![Extrait d’une carte de Inō Tadataka](/blog/cocarto_12/tadataka.webp)

Drôle de crise de la cinquantaine pour  [Inō Tadataka](https://fr.wikipedia.org/wiki/In%C5%8D_Tadataka) : il était le dirigeant d’une brasserie de saké, mais après 5 ans de formation il se consacrera jusqu’à sa mort en 1818 à création de la première carte « moderne » du Japon, parcourant plus de 30 000km à pied.

Son œuvre a été achevée en 1821 par ses étudiants puis gardée cachée par le gouvernement jusqu’à la fin de l’[époque d’Edo](https://fr.wikipedia.org/wiki/%C3%89poque_d%27Edo). Les cartes ont ensuite été utilisées jusqu’en 1924.

Une copie a été découverte en 2001 à la _Library of Congress_. Elle est [disponible en ligne](https://www.loc.gov/item/2001620020) pour que vous puissiez admirer 207 des 214 cartes qui font 86 × 104 cm à l’échelle 1:43 600.

Et pour en savoir un peu plus sur son parcours, je vous invite à lire [cet article sur nippon.com](https://www.nippon.com/en/japan-topics/b07210/mapping-a-nation-japan%E2%80%99s-most-famous-cartographer-ino-tadataka.html).

##

C’est tout pour cette fois-ci ! C’est probablement la dernière lettre de l’année. Merci encore de nous avoir lus. N’hésitez pas à nous envoyer vos commentaires sur [bonjour@cocarto.com](mailto:bonjour@cocarto.com), à regarder la [roadmap](https://gitlab.com/CodeursEnLiberte/cocarto/-/milestones/19), et à nous signaler des bugs.
