+++
date = "2024-12-06T18:00:00+02:00"
title = "Une suite d’icônes dans un fichier svg"
author = "Nicolas"
categories = "Technique"
+++

On a redesigné le site de codeur·euses en Liberté ! C’est vrai que depuis 8 ans, ça commençait à prendre un peu la poussière. C’est un peu plus frais. Il y a encore quelques morceaux en mouvement, n’hésitez pas à nous <svg class="icon"><use href="/img/icons.svg#fa-envelope"/></svg> <a href="mailto:bonjour@codeureusesenliberte.fr">signaler</a> les petits bugs.

Le plus visible, c’est le __nouveau logo__ 🙀:

<img style="width: 200px; outline: none" src="../../logo.svg" alt="Le nouveau logo codeur·euses en liberté">

Il ressemble à celui d’avant, mais son style reflète bien notre usage des dernières technologies.

<figure style="max-width: 500px">
  <img src="machine.webp" alt="Le logo de Codeur·euses en liberté sur une feuille dans une machine à écrire.">
  <figcaption>En route pour le futur</figcaption>
</figure>

C’était aussi l’occasion pour moi de faire un peu de _frontend_, c’est-à-dire du CSS, du HTML, et du ~JS~[^1]SVG. Voici donc le premier post d’une série de trois, où je raconte les petits hacks, les techniques « modernes », et les bonnes ou mauvaises pratiques découvertes à l’occasion.

[^1]: D’accord, il y a un poil de javascript, mais c’est pour débugguer le CSS.

## Comment faire une suite d’icônes dans un unique fichier svg

Un des chantiers à l’occasion du redesign a été de simplifier la gestion des icônes du site, en permettant de les teinter avec le texte. Il s’agit du problème habituel : une suite de glyphes/symboles/icônes, qu’on veut pouvoir utiliser dans le corps du texte ou dans des boutons.

Je vous livre déjà la conclusion : **ne faites pas de polices d’icônes**, c’était déjà pénible il y a dix ans, et ça fait longtemps que ça ne sert plus à rien.

## Le problème

Avant le redesign, on utilisait déjà une poignée d’icônes, tirée de FontAwesome. Ça ressemblait à ça:

```css
.icon::before {
  display: inline-block;
  margin-inline-end: 0.5ch; 
  content: ' '; /* a single unbreakable space */
  width: 1.5ch;
  background: no-repeat center center;
  background-size: contain;
}

.icon-envelope::before { background-image: url('../img/fa-envelope.svg') }
.icon-github::before   { background-image: url('../img/fa-github.svg') }
.icon-gitlab::before   { background-image: url('../img/fa-gitlab.svg') }
.icon-heart::before    { background-image: url('../img/fa-heart.svg') }
.icon-key::before      { background-image: url('../img/fa-key.svg') }
.icon-mastodon::before { background-image: url('../img/fa-mastodon.svg') }
.icon-web::before      { background-image: url('../img/fa-globe.svg') }
```
Avec autant de fichiers svg que d’icônes, et autant de règles css à répéter.  
Dans le html, ça s’utilisait ainsi :
```html
<p>
  Contactez-nous par email à
  <a class="icon icon-envelope" href="mailto:bonjour@codeureusesenliberte.fr">bonjour@codeureusesenliberte.fr</a>
</p>
```

Ça marchait pas mal :

<img style="width: 235px" src="old-light.webp" alt="">

Mais en mode sombre, rien n’allait plus:

<img style="width: 235px" src="old-dark.webp" alt="">

Par ailleurs, d’un point de vue accessibilité[^2], c’était pas génial : il n’y avait pas trace de l’image dans le DOM. On pouvait faire mieux.

[^2]: Et sémantique, mais c’est un peu pareil.

## Teinter des svg avec la couleur du texte

Faire de la couleur en svg, c’est pas très compliqué, c’est selon le cas l’attribut [`fill` ou l’attribut `stroke`](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Fills_and_Strokes). Par exemple, ce fragment de html:

```html
<blockquote style="color: darkblue">
  Du texte bleu avant un rond orange: 
  <svg width=10px height=10px>
    <circle cx="5" cy="5" r="5" fill="orange"/>
  </svg>
</blockquote>
```

Donne cela:

<blockquote style="color: darkblue">
  Du texte bleu avant un rond orange: 
  <svg width=10px height=10px>
    <circle cx="5" cy="5" r="5" fill="orange"/>
  </svg>
</blockquote>

On peut varier, la couleur du texte et la couleur du rond indépendamment:

<blockquote style="color: darkred">
  Du texte bleu avant un rond bleu: 
  <svg width=10px height=10px>
    <circle cx="5" cy="5" r="5" fill="lightblue"/>
  </svg>
</blockquote>

Mais ce qu’on veut, c’est que le svg soit dessiné avec la couleur du texte. L’astuce pour consiste à utiliser le mot-clé [`currentcolor`](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#currentcolor_keyword):

```html
<blockquote style="color:darkgreen">
  Du texte bleu avant un rond de la même couleur : 
  <svg width=10px height=10px>
    <circle cx="5" cy="5" r="5" fill="currentcolor" /> <!-- ici -->
  </svg>
</blockquote>
```

Ça donne ça :

<blockquote style="color:darkgreen">
  Du texte bleu avant un rond de la même couleur : 
  <svg width=10px height=10px>
    <circle cx="5" cy="5" r="5" fill="currentcolor"/> <!-- ici -->
  </svg>
</blockquote>


## Tous les svg dans le même fichier

Si vos symboles se limitent à des petits ronds, félicitations, c’est fini ! Comme le svg des symboles est souvent nettement plus complexe et est composé d’un ou plusieurs `path`  avec leur syntaxe [un poil absconse](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths), on préfère éviter de les répéter dans le DOM à chaque utilisation.

___svg `use` à la rescousse___

Le langage SVG permet de déclarer des _symboles_ et de leur donner un identifiant:

```html
<svg>
<symbol id="icon_name">
  ...
</symbol>
</svg>
```

On peut ensuite les utiliser avec l’élément `use`:

```html
<svg><use href="#icon_name"/></svg>
```

Tous les symboles svg peuvent être réunis dans un seul fichier. Ça donne ça, avec l’icône de mastodon, chargée depuis le fichier [icons.svg](/img/icons.svg):

```html
<svg width=20px height=20px><use href="/img/icons.svg#fa-mastodon"/></svg>
```

<blockquote style="color:darkcyan">
  Un éléphant <svg width=20px height=20px><use href="/img/icons.svg#fa-mastodon"/></svg> ça trompe énormément.
</blockquote>

## Quelques détails techniques pour finir:

- Dans le [fichier svg](/img/icons.svg), nous avons effectivement mis les icônes dans des éléments [`<symbol>`](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/symbol), eux-mêmes réunis dans un élément [`<defs>`](https://developer.mozilla.org/en-US/docs/Web/SVG/Element/defs). Ces deux éléments n’ont pas d’incidence technique : on obtiendrait le même résultat avec des `g`, mais comme dit la MDN: they <q>_promote understandability of the SVG content and is beneficial to the overall accessibility of the document_</q>.

- Note: le contenu du svg est _shadowed_ dans le DOM de la page, ce qui signifie qu’il n’est pas accessible au JS ou au CSS. Autrement dit, vous ne pouvez pas appliquer un style CSS aux symboles. Cependant, `currentcolor` est bien pris en compte! Si on veut plus de liberté, il faut embarquer le svg _dans le HTML_. Là, vous pouvez faire tout le css que vous voudrez.

Comme je le disais au début, ce n’est pas une technique moderne ! C’est supporté par tous les navigateurs depuis [une bonne dizaine d’années](https://caniuse.com/mdn-svg_elements_use). Malheureusement, un bon paquet des [collections](https://docs.fontawesome.com/web/setup/get-started) [d’icônes](https://github.com/phosphor-icons/homepage?tab=readme-ov-file#vanilla-web) [largement utilisées](https://developers.google.com/fonts/docs/material_symbols?hl=fr) sont basées sur l’utilisation de polices de symboles. C’est d’ailleurs pour ça que FontAwesome s’appelle comme ça — mais ça ne nous empêche pas d’utiliser les glyphes de FontAwesome dans des svg.[^3] 

[^3]: FontAwesome “free” est [distribué](https://fontawesome.com/license/free) en CC BY SA 4.0. J’ai le vague sentiment que le gros du travail de FontAwesome en 2024 est de faire de l’outillage et de la gestion de paquets, pas de dessiner des icônes. Gardons ce débat pour un autre jour. 

C’est tout pour cette fois-ci: la prochaine fois, on parlera de _variables css_ et de _design adaptatif_ __sans media queries__. 
