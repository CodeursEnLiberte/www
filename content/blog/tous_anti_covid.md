+++
date = "2024-02-13T14:47:27+02:00"
title = "TousAntiCovid : vu depuis Codeureuses en Liberté"
author = "Codeureuses en Liberté"
categories = "Entreprise"
+++

TousAntiCovid a refait récemment l’[actualité](https://www.huffingtonpost.fr/justice/article/application-stopcovid-la-justice-enquete-sur-des-soupcons-de-favoritisme_181078.html) sur les montants de l’hébergement et les modalités d’attribution du marché. À l’époque, Codeureuses en Liberté avait été approchée pour envisager la réalisation de cette application. On vous livre ici nos reflexions de l’époque.

Au début de l’épidémie de covid-19, il y a eu la croyance qu’une solution informatique pourrait être utile pour atténuer l’engorgement des hôpitaux grace au [contact tracing (recherche de contacts)](https://fr.wikipedia.org/wiki/Recherche_des_contacts). L’état français a donc voulu une application qui deviendrait [TousAntiCovid](https://fr.wikipedia.org/wiki/TousAntiCovid) (initialement _StopCovid_).

En 2020, Codeureuses en Liberté est co-attributaire[^la_zone] d’un contrat cadre avec la [Direction interministérielle du numérique (DINUM)](https://www.numerique.gouv.fr/dinum/)[^bou_hana]. Dans ce cadre, nous étions souvent intermédiaires d’autres indépendant·e·s qui intervenaient sur des [startup d’état](https://beta.gouv.fr/startups/) de _beta.gouv.fr_[^sous-traitance].

Le 11 avril 2020, la DINUM nous contacte et s’ensuivent des discussions sur notre messagerie :

> <cite>Tristram</cite>  
> Question assez importante : [notre contact à beta] demande quelle serait notre position sur porter les développement de l’app stop covid.  
> À première vue, j’ai du mal à me convaincre qu’on obtiendra des garanties suffisantes sur le respect de la vie privée, de l’autre c’est peut-être l’occasion de forcer (dans le devis, vis-à-vis des sous-traitants) de s’assurer qu’il n’y aura aucune donnée stockée, que le développement soit libre depuis le jour 1.  
> `@here` je me permet une notif, vu la complexité éthique du sujet et l’urgence (une réponse mardi serait souhaitée)
> 
> <cite>Vincent</cite>  
> En fait j'aimerai bien avoir l'avis d'un infectiologue, je vois passer beaucoup de personnes sur les libertés publiques mais rien de l'avis de médecin.
> 
> <cite>Pierre</cite>  
> Je pense qu'on peut peut-être faire légèrement moins pire que d'autres, mais que de toute manière, entre Google/Apple et le gouvernement, on aura une marge de manœuvre à peu près nulle.
> 
> <cite>Vincent</cite>  
> J'ai l'impression qu'en refusant on se donne le beau rôle de «non ça c'est pas pour moi» mais bon on sait très bien qu’[une ESN] va le prendre derrière.
> 
> <cite>Tristram</cite>  
> Il y a un risque de dérive et c’est là qu’on pourra ralentir, surtout en posant à chaque bon de commande des clauses et en étant très clair qu’on arrête tout si on dérive. Ce qui aurait l’avantage de ralentir pas mal, car ils devront retrouver d’autres personnes pour le faire.
> 
> <cite>Nicolas</cite>  
> Si on fait juste du portage, on aura pas notre mot à dire sur le produit; est-ce que vous proposez qu’on lâche nos projets actuels pour nous consacrer à ça? 
> 
><cite>Kheops</cite>  
> J'ai presque l'impression que le simple fait qu'on soit mis devant ce choix mérite déjà qu'on publie quelque chose à ce propos.
> 
> <cite>Thibaut</cite>  
> L’application ne sera pas suffisante en elle-même pour juguler l’épidémie, ça serait trop beau. Mais elle peut être un outil efficace pour éviter des décès.  
> L’application ne pourra pas ne pas exister, donc donnons lui le meilleur cadre de développement possible.  
> Si elle devient obligatoire pour les possésseurs de smartphone, et que c’est finalement le consortium qui la développe, j’imagine qu’on aura des regrets.


Après plein d’autres débats, de visios, nous avions fini par décider de participer en posant nos conditions. Voici l’email que nous nous préparions à envoyer.

> Nous exigeons de pouvoir communiquer publiquement (blog post sur notre site)sur  ce  projet dès la fin des débats parlementaires.
>
> Le blog post reprendra les lignes rouges suivantes, qui amènerait à ne pas reconduire le portage et a publier le refus de continuer : L’application et le serveur ne doivent servir qu’à lutter contre l’épidémie de coronavirus.
>
> On ne doit récolter que le minimum nécessaire de données. Aucun moyen de géolocalisation ne sera utilisé (par exemple GPS).
>
>Aucun identifiant ni identité ne sera associée à l’application.
>
>Les données sur le téléphone et sur le serveur ne seront conservées qu’une durée définie explicitement et argumentée (justifiée tant par l'état des connaissances médicales que par le besoin de respect de la vie privée).
>
>Les données utilisateurs ne seront transmises sur le serveur central qu’avec une action explicite et informée de l’utilisateur.
>
>Le code source intégral est publié sous une licence libre avant le déploiement et par la suite l'intégralité du processus de développement sera public.
>
>L’installation de l'application ne doit pas être discriminante et donc ne doit ni être un devoir ni conditionner l'accès à des droits (tel que le droit de se déplacer librement, ou donner l'exclusivité de l'accès aux tests du CoVid-19).
>
>L’appli ne doit pas embarquer de SDK d’analytics.
>
>Elle passera le crible d’exodus privacy sans problème.

Ces discussions — comme toute discussion collective sur un sujet complexe — ont pris du temps. Cela n’a pas été facilité par la coordination avec le groupement, la DINUM et surtout l’évocation d’un accord de confidentialité. Au moment où nous allions envoyer cet email, nous apprenions que le projet était finalement confié au [consortium de l’angoisse](https://www.capgemini.com/fr-fr/actualites/communiques-de-presse/stopcovid/) qui inclut Capgemini, Orange, Dassault Systèmes, Withing, d’autres structures privées et publiques, et, « dans l’écosystème », Atos, Accenture, Thalès ou encore Sopra Steria.

Il n’y a pas vraiment de conclusion à tout ça. Nous avons été témoins, par le petit bout de la lorgnette, du tout début de cette histoire; nous avons imaginé un instant avoir l‘opportunité, ou le devoir, d’y faire quelque chose. Finalement, nous n’avons jamais vraiment donné notre avis. 🤷 

[^la_zone]: Il y avait trois attributaires. Codeurseures en Liberté était membre d’un groupement informellement appelé _La Zone_ qui était un des trois attributaires. Nous ne parlons ici qu’en notre nom.
[^sous-traitance]: De nombreuses indépendant·e·s souhaitaient travailler avec beta.gouv.fr et le recrutement directe reste une question complexe.
[^bou_hana]: à ce moment dirigée par le [très problématique](https://www.lemonde.fr/politique/article/2021/12/08/changement-de-tete-a-la-direction-interministerielle-du-numerique-en-pleine-crise-interne_6105148_823448.html) Nadi Bou Hana.