+++
date = "2022-07-01T08:18:00+02:00"
title = "Ĝis : billet d’étape n°2 juin 2022"
author = "Tristram"
categories = "cocarto"
+++

[Ĝis](https://gxis.codeursenliberte.fr) est un outil de saisie de données cartographique et territoriale. Vous lisez la deuxième édition de la lettre mensuelle qui en raconte les développements, les doutes, détails inutiles…

 ![Voyage géodésie pour l’Équateur](/blog/gxis_juin/voyage_geodesie_equateur.jpg)

# Que s’est-il passé en juin ?

Nous n’avons toujours pas trouvé un nom. Il faudra qu’on se décide afin de publier une version alpha.

En plus d’un travail de fond sur l’esthétique, les parcours de l’utilisateur, l’outillage (linters, analyse automatique de sécurité…), nous avons ajouté :

* **La gestion des polygones** (tracé et modification), avec bien évidemment une synchronisation si une autre personne travaille en même temps sur la même couche de données ;
* **L’export des données** d’une couche au format GeoJSON ;
* **Des _cartes_** qui regroupent plusieurs couches ;
* **Un type de colonne _Date_ et _Booléen_**.

![Exemple de Ĝis représentant des points du méridien de Paris](/blog/gxis_juin/gxis_meridien.png)

# Le financement

[Codeurs en Liberté](https://codeursenliberte.fr) est une petite coopérative en informatique de 10 personnes. Nous avons accumulé assez d’argent et à notre [assemblée générale](https://www.codeursenliberte.fr/blog/ag_2021/) nous avons décidé d’investir 50 000 € en fond propre pour écrire un prototype et tester la pertinence du produit.

Au delà, nous aimerions bien gagner de l’argent avec ce produit.

Nous sommes une coopérative, pas une startup. La valeur de nos actions est fixe et — légalement — plus de la moitié  (aujourd’hui 100%) du capital sera toujours détenue par les travailleurs et travailleuses.

Nous ne ferons donc pas de levée d’argent et ne pourrons pas être vendu plusieurs millions. Et c’est tant mieux : combien de produits et d’équipes que nous avons aimées ont été détruites suite à un rachat ?

Le [code source](https://gitlab.com/CodeursEnLiberte/gxis/) est totalement libre (licence AGPL) et tout notre développement ouvert. Nous ne voulons pas faire de double licence avec une version payante.

Pour récupérer des sous, nous souhaitons lancer une offre commerciale hébergée, mais aussi proposer des formations, accompagner l’intégration de données et nous faire financer des fonctionnalités pour des besoins spécifiques.

# Un focus technique

La carte est l’élément le plus visible. Les données qui génèrent le fond de carte sont issues de la base collaborative [OpenStreetMap](https://www.openstreetmap.org/#map=10/47.5385/9.5622).

Pour les afficher sur votre navigateur, historiquement, des images (« tuiles ») étaient générées par un serveur puis envoyées et affichées.

Depuis, ce sont juste les tracés des routes et des bâtiments qui sont envoyés (« [tuiles vectorielles](https://en.wikipedia.org/wiki/Vector_tiles) »). C’est le navigateur qui va faire le rendu, permettant d’avoir un zoom beaucoup plus fluide et beaucoup plus de souplesse sur le choix esthétiques pour le rendu.

C’est la bibliothèque [maplibre](https://maplibre.org/) qui se charge de télécharger les tuiles vectorielles et de faire le rendu avec le style que nous avons spécifié.

# Cartographe du mois : Pierre Bouguer

En 1735, pour déterminer la forme de la Terre (spoiler: une orange écrasée aux pôles), la France lance une [grande expédition géodésique](https://fr.wikipedia.org/wiki/Exp%C3%A9ditions_g%C3%A9od%C3%A9siques_fran%C3%A7aises) vers l’Équateur (qui doit son nom à cette expédition). L’expédition durera 10 ans.

Pierre Bouguer en a fait parti et à rédigé _La Figure de la Terre_ suite à cette expédition. Il aurait également introduit les symboles ⩾ et ⩽.

L’expédition en elle même est incroyable et nous vous invitons vivement à lire deux livres qui en parlent, et qui se lisent comme des romans d’aventure :

* [Le procès des étoiles](https://www.lalibrairie.com/livres/le-proces-des-etoiles--recit-de-la-prestigieuse-expedition-de-trois-savants-francais-en-amerique-du-sud-et-des-aventures-qui-s-ensuivirent--1735-1771_0-4318571_9782228918749.html?ctx=11359304d4de4baf76591a256585b902), de Florence Trystram
* [La femme du cartographe](https://www.lalibrairie.com/livres/la-femme-du-cartographe--une-histoire-vraie-d-amour-de-meurtre-et-de-survie-en-amazonie_0-4694187_9782228920360.html?ctx=34f9d69ea7b0c45a426a8043d551b93d), de Robert Whitaker

# Conclusion

Nous sommes toujours preneurs de retours, rapports de bugs, besoins concrets et d’idées pour un nom. N’hésitez pas à nous écrire !
