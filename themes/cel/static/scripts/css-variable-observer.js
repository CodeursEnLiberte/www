// CSS variable observer. Display the live value of CSS dimension variables.
// 
// Why is this needed? CSS variables are only evaluated when they are used in an actual CSS property.
// When properties are computed from variables that themselves depend on other variables, it can be hard to understand what’s going on.
// 
// This works by:
// 1. creating a hidden div whose width depends on the value of the observed variable,
// 2. adding a ResizeObserver on this element.
// 
// Note: We need to add an (arbitrary) 10000px width.
//   The observed variables can become negative, but the width of an element can’t be less than zero.

function observeCssVariable(variable_name) {
  const offset = 10000
  
  let element = document.createElement("div")
  element.style.width = `calc(${offset}px + var(${variable_name}))`
  document.body.appendChild(element)
  new ResizeObserver(() => {
    const value = element.getBoundingClientRect().width - offset
    console.debug(`${variable_name} = ${value}`)
  }).observe(element)
}
