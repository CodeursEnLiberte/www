Ceci est le code-source du site web de Codeur·euses en Liberté (www.codeureusesenliberté.fr).

Le site est généré par [Hugo](http://gohugo.io/) à partir de fichiers HTML et Markdown.
Il contient à la fois les pages statiques du site (liste des sociétaires, page « À propos »)
et des articles de blog.

## Comment développer en local

1. [Installer hugo](https://gohugo.io/getting-started/installing/) ;
2. `make run` pour builder le site et lancer un serveur local.

## Comment écrire un article de blog

Il y a deux façons possibles :

- **Depuis l'interface de GitLab** : cliquez **[sur ce lien](https://gitlab.com/CodeursEnLiberte/www/-/new/main/content%2Fblog)** pour écrire et ajouter un article directement depuis GitLab, sans avoir à installer le blog sur votre ordinateur. Vous pouvez utiliser la syntaxe `_Markdown_` ou `<em>HTML</em>`. Inspirez-vous [d’un article existant](https://gitlab.com/CodeursEnLiberte/www/-/blob/main/content/blog/débuter.md) si besoin.  Quand vous avez terminé, commitez l’article depuis l'éditeur et ouvrez une nouvelle merge request (vous pourrez toujours faire des changements après).

- **Sur votre ordinateur** : installez le site en local (voir ci dessous), puis ajoutez un fichier Markdown dans le dossier `content/blog/`.

## Comment générer le site en local

Lancer `make build`. Le site est généré dans le dossier `public/`.

## Comment déployer le site

Le site est déployé avec Gitlab pages, configuré dans `.gitlab-ci.yml`. De façon classique, le site est déployé automatiquement par gitlab-ci quand un nouveau commit est poussé sur la branche `main`.
